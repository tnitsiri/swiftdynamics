class PersonModel {
  String id;
  String firstName;
  String lastName;
  String about;
  DateTime createdAt;
  DateTime updatedAt;

  PersonModel({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.about,
    required this.createdAt,
    required this.updatedAt,
  });

  factory PersonModel.fromJson(Map<String, dynamic> json) => PersonModel(
    id: json['id'],
    firstName: json['firstName'],
    lastName: json['lastName'],
    about: json['about'],
    createdAt: DateTime.parse(json['createdAt']).toLocal(),
    updatedAt: DateTime.parse(json['updatedAt']).toLocal(),
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'firstName': firstName,
    'lastName': lastName,
    'about': about,
    'createdAt': createdAt.toIso8601String().toString(),
    'updatedAt': updatedAt.toIso8601String().toString(),
  };
}