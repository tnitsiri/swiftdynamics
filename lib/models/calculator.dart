import 'package:swiftdynamics/enums/calculator.dart';

class CalculatorModel {
  String id;
  CalculatorState state;
  String? number;
  CalculatorOperator? operator;

  CalculatorModel({
    required this.id,
    required this.state,
    this.number,
    this.operator,
  });

  factory CalculatorModel.fromJson(Map<String, dynamic> json) => CalculatorModel(
    id: json['id'],
    state: json['state'],
    number: json['number'],
    operator: json['operator'],
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'state': state,
    'number': number,
    'operator': operator,
  };
}