enum CalculatorState {
  Number,
  Operator,
  Error,
}

enum CalculatorOperator {
  Addition,
  Subtraction,
  Multiplication,
  Division,
}