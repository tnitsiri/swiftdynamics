import 'package:intl/intl.dart';

String displayPrice(double price) {
  if (price.toString().contains('e+') || price.toString().contains('e-')) {
    return price.toString();
  }
  return '${(price % 1 == 0 ? NumberFormat().format(price) : NumberFormat().format(double.parse(price.toStringAsFixed(2))))}';
}