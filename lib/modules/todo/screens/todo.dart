import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:objectdb/objectdb.dart';
import 'package:path_provider/path_provider.dart';
import 'package:swiftdynamics/models/todo.dart';
import 'package:swiftdynamics/modules/todo/components/item.dart';
import 'package:uuid/uuid.dart';
// ignore: implementation_imports
import 'package:objectdb/src/objectdb_storage_filesystem.dart';

class TodoScreen extends StatefulWidget {
  @override
  State createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  final Uuid _uuid = Uuid();
  final ScrollController _scrollController = ScrollController();
  final TextEditingController _controller = TextEditingController();
  late ObjectDB _db;
  bool _showCompleted = false;
  List<TodoModel> _items = [];

  // add todo list
  void _add(String value) async {
    String text = value.trim();

    if (text.isEmpty) {
      return;
    }

    String id = _uuid.v1();

    TodoModel item = TodoModel(
      id: id,
      text: text,
      isCompleted: false,
    );

    if (mounted) {
      setState(() {
        _controller.text = '';
        _items.add(item);
      });
    }

    await _db.insert({
      'id': id,
      'text': text,
      'isCompleted': false,
    });
  }

  // complete
  void _complete(TodoModel item) async {
    int index = _items.indexWhere((e) => e.id == item.id);

    if (index > -1) {
      bool isCompleted = !_items[index].isCompleted;

      if (mounted) {
        setState(() {
          _items[index].isCompleted = isCompleted;
        });
      }

      await _db.update({
        'id': item.id,
      }, {
        'isCompleted': isCompleted,
      });
    }
  }

  // remove item
  void _remove(TodoModel item) async {
    if (mounted) {
      setState(() {
        _items.removeWhere((e) => e.id == item.id);
      });
    }

    await _db.remove({
      'id': item.id,
    });
  }

  // init database
  void _initDb() async {  
    try {
      Directory docDic = await getApplicationDocumentsDirectory();
      String dbPath = docDic.path + '/todo.db';
      FileSystemStorage storage = FileSystemStorage(dbPath);
  
      _db = ObjectDB(storage);

      List<dynamic> data = await _db.find();
      List<TodoModel> items = data.map((d) => TodoModel.fromJson(d)).toList();

      if (mounted) {
        setState(() {
          _items = items;
        });
      }
    }
    catch (_) {
    }
  }

  void _unfocus() {
    FocusScope.of(context).unfocus();
    FocusScope.of(context).requestFocus(FocusNode());
  }

  @override
  void initState() {
    _initDb();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Todo'),
        trailing: CupertinoButton(
          minSize: 0,
          padding: EdgeInsets.zero,
          borderRadius: BorderRadius.zero,
          child: Text(_showCompleted ? 'Hide Completed' : 'Show Completed',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          ),
          onPressed: () {
            if (mounted) {
              setState(() {
                _showCompleted = !_showCompleted;
              });
            }
          },
        ),
      ),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // start: list
            Expanded(
              child: NotificationListener<ScrollNotification>(
                onNotification: (ScrollNotification scrollInfo) {
                  if (_scrollController.position.userScrollDirection == ScrollDirection.reverse) {
                    _unfocus();
                  }

                  return true;
                },
                child: CustomScrollView(
                  controller: _scrollController,
                  physics: AlwaysScrollableScrollPhysics(),
                  slivers: [
                    SliverFillRemaining(
                      hasScrollBody: false,
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            ..._items
                              .where((e) => _showCompleted ? true : !e.isCompleted)
                              .map((e) => TodoItemComponent(
                                key: ValueKey(e.id),
                                item: e,
                                onComplete: () => _complete(e),
                                onRemove: () => _remove(e),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // end: list
            // start: form
            Container(
              decoration: BoxDecoration(
                color: Color(0xFFE1E4E8),
              ),
              padding: EdgeInsets.only(
                top: 10,
                bottom: MediaQuery.of(context).padding.bottom + 10,
                left: 12,
                right: 12,
              ),
              child: CupertinoTextField(
                controller: _controller,
                autofocus: true,
                placeholder: 'Add new todo list...',
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.sentences,
                textInputAction: TextInputAction.done,
                clearButtonMode: OverlayVisibilityMode.editing,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(100),
                ],
                style: TextStyle(
                  fontSize: 15,
                ),
                placeholderStyle: TextStyle(
                  fontSize: 15,
                  color: Color(0xFFCCCCCC),
                ),
                padding: EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: 12,
                ),
                onEditingComplete: () {},
                onSubmitted: _add,
              ),
            ),
            // end: form
          ],
        ),
      ),
    );
  }
}