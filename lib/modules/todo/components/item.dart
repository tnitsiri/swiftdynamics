import 'package:flutter/cupertino.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:swiftdynamics/models/todo.dart';

class TodoItemComponent extends StatelessWidget {
  final Key key;
  final TodoModel item;
  final VoidCallback onComplete;
  final VoidCallback onRemove;

  TodoItemComponent({
    required this.key,
    required this.item,
    required this.onComplete,
    required this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Color(0xFFE9E9E9),
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: CupertinoButton(
              minSize: 0,
              borderRadius: BorderRadius.zero,
              padding: EdgeInsets.zero,
              child: Container(
                padding: EdgeInsets.only(
                  top: 5,
                  bottom: 5,
                  left: 12,
                  right: 10,
                ),
                child: Center(
                  child: Icon(item.isCompleted ? MaterialCommunityIcons.circle_slice_8 : MaterialCommunityIcons.circle_outline,
                    size: 23,
                    color: item.isCompleted ? CupertinoColors.systemBlue : Color(0xFFD6D6D6),
                  ),
                ),
              ),
              onPressed: onComplete,
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(
                vertical: 12,
              ),
              child: Text(item.text,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 16,
                  color: CupertinoTheme.of(context).textTheme.textStyle.color,
                ),
              ),
            ),
          ),
          CupertinoButton(
            minSize: 0,
            borderRadius: BorderRadius.zero,
            padding: EdgeInsets.zero,
            child: Container(
              padding: EdgeInsets.only(
                top: 5,
                bottom: 5,
                left: 10,
                right: 12,
              ),
              child: Center(
                child: Icon(FontAwesome.trash_o,
                  size: 21,
                  color: CupertinoColors.systemRed,
                ),
              ),
            ),
            onPressed: onRemove,
          ),
        ],
      ),
    );
  }
}