import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class PeopleFieldComponent extends StatelessWidget {
  final String label;
  final String placeholder;
  final FocusNode focusNode;
  final TextEditingController controller;
  final TextInputAction textInputAction;
  final int? minLines;
  final int? maxLines;
  final Function(String)? onSubmitted;
  final int maxLength;
  final TextInputType keyboardType;
  final bool autofocus;

  PeopleFieldComponent({
    required this.label,
    required this.placeholder,
    required this.focusNode,
    required this.controller,
    required this.textInputAction,
    this.minLines,
    this.maxLines,
    this.onSubmitted,
    this.maxLength = 50,
    this.keyboardType = TextInputType.text,
    this.autofocus = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 15,
      ),
      padding: EdgeInsets.only(
        left: 12,
        right: 12,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(label,
            style: TextStyle(
              fontSize: 13.5,
              fontWeight: FontWeight.w600,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 8,
            ),
            child: CupertinoTextField(
              focusNode: focusNode,
              controller: controller,
              placeholder: placeholder,
              keyboardType: keyboardType,
              textCapitalization: TextCapitalization.sentences,
              textInputAction: textInputAction,
              autofocus: autofocus,
              inputFormatters: [
                LengthLimitingTextInputFormatter(maxLength),
              ],
              style: TextStyle(
                fontSize: 15,
              ),
              placeholderStyle: TextStyle(
                fontSize: 15,
                color: Color(0xFFCCCCCC),
              ),
              padding: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 12,
              ),
              minLines: minLines,
              maxLines: maxLines,
              onSubmitted: onSubmitted,
            ),
          ),
        ],
      ),
    );
  }
}