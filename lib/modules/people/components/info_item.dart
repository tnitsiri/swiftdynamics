import 'package:flutter/cupertino.dart';

class PeopleInfoItemComponent extends StatelessWidget {
  final String label;
  final String value;

  PeopleInfoItemComponent({
    required this.label,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Color(0xFFE9E9E9),
          ),
        ),
      ),
      padding: EdgeInsets.symmetric(
        vertical: 13,
        horizontal: 12,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(
              right: 12,
            ),
            child: Text(label,
              style: TextStyle(
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Expanded(
            child: Text(value,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }
}