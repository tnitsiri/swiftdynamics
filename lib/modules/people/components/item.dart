import 'package:flutter/cupertino.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:swiftdynamics/models/person.dart';

class PeopleItemComponent extends StatelessWidget {
  final PersonModel item;
  final VoidCallback onView;
  final VoidCallback onRemove;

  PeopleItemComponent({
    required this.item,
    required this.onView,
    required this.onRemove,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Color(0xFFE9E9E9),
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: CupertinoButton(
              minSize: 0,
              padding: EdgeInsets.zero,
              borderRadius: BorderRadius.zero,
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(
                  top: 12,
                  bottom: 12,
                  left: 12,
                ),
                child: Text('${item.firstName} ${item.lastName}',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 16,
                    color: CupertinoTheme.of(context).textTheme.textStyle.color,
                  ),
                ),
              ),
              onPressed: onView,
            ),
          ),
          CupertinoButton(
            minSize: 0,
            borderRadius: BorderRadius.zero,
            padding: EdgeInsets.zero,
            child: Container(
              padding: EdgeInsets.only(
                top: 5,
                bottom: 5,
                left: 10,
                right: 12,
              ),
              child: Center(
                child: Icon(FontAwesome.trash_o,
                  size: 21,
                  color: CupertinoColors.systemRed,
                ),
              ),
            ),
            onPressed: onRemove,
          ),
        ],
      ),
    );
  }
}