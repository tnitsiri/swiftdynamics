import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:objectdb/objectdb.dart';
import 'package:oktoast/oktoast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:swiftdynamics/modules/people/components/field.dart';
// ignore: implementation_imports
import 'package:objectdb/src/objectdb_storage_filesystem.dart';
import 'package:uuid/uuid.dart';

class PeopleFormScreen extends StatefulWidget {
  @override
  State createState() => _PeopleFormScreenState();
}

class _PeopleFormScreenState extends State<PeopleFormScreen> {
  final Uuid _uuid = Uuid();
  final FocusNode _firstNameFocusNode = FocusNode();
  final FocusNode _lastNameFocusNode = FocusNode();
  final FocusNode _aboutFocusNode = FocusNode();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _aboutController = TextEditingController();
  late ObjectDB _db;
  bool _doing = false;
  bool _done = false;

  // save
  void _save() async {
    if (_doing || _done) {
      return;
    }

    if (_firstNameController.text.trim().isEmpty) {
      showToast('Please specify first name.');
    }
    else if (_lastNameController.text.trim().isEmpty) {
      showToast('Please specify last name.');
    }
    else if (_aboutController.text.trim().isEmpty) {
      showToast('Please specify about.');
    }
    else {
      _unfocus();

      if (mounted){
        setState(() {
          _doing = true;
        });
      }

      try {
        DateTime now = DateTime.now();

        await _db.insert({
          'id': _uuid.v1(),
          'firstName': _firstNameController.text.trim(),
          'lastName': _lastNameController.text.trim(),
          'about': _aboutController.text.trim(),
          'createdAt': now.toIso8601String(),
          'updatedAt': now.toIso8601String(),
        });

        if (mounted) {
          setState(() {
            _done = true;
          });
        }

        showToast('The data was saved successfully.');
        Navigator.of(context).pop(true);
      }
      catch (_) {
        print(_);
        showToast('An error occurred during execution.');
      }
      finally {
        if (mounted) {
          setState(() {
            _doing = false;
          });
        }
      }
    }
  }

  // init database
  void _initDb() async {  
    try {
      Directory docDic = await getApplicationDocumentsDirectory();
      String dbPath = docDic.path + '/people.db';
      FileSystemStorage storage = FileSystemStorage(dbPath);
  
      _db = ObjectDB(storage);
    }
    catch (_) {
    }
  }

  void _unfocus() {
    FocusScope.of(context).unfocus();
    FocusScope.of(context).requestFocus(FocusNode());
  }

  @override
  void initState() {
    _initDb();
    super.initState();
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _aboutController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Add Person'),
        trailing: CupertinoButton(
          minSize: 0,
          padding: EdgeInsets.zero,
          borderRadius: BorderRadius.zero,
          child: Text(_doing ? 'Saving...' : 'Save'),
          onPressed: _doing ? null : _save,
        ),
      ),
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            top: 15,
            bottom: MediaQuery.of(context).padding.bottom,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              PeopleFieldComponent(
                label: 'First name*',
                placeholder: 'Specify first name',
                focusNode: _firstNameFocusNode,
                controller: _firstNameController,
                textInputAction: TextInputAction.next,
                autofocus: true,
                onSubmitted: (String value) => FocusScope.of(context).requestFocus(_lastNameFocusNode),
              ),
              PeopleFieldComponent(
                label: 'Last name*',
                placeholder: 'Specify last name',
                focusNode: _lastNameFocusNode,
                controller: _lastNameController,
                textInputAction: TextInputAction.next,
                onSubmitted: (String value) => FocusScope.of(context).requestFocus(_aboutFocusNode),
              ),
              PeopleFieldComponent(
                label: 'About*',
                placeholder: 'Specify information about the person',
                focusNode: _aboutFocusNode,
                controller: _aboutController,
                textInputAction: TextInputAction.newline,
                keyboardType: TextInputType.multiline,
                maxLength: 100,
                minLines: 3,
                maxLines: 3,
              ),
            ],
          ),
        ),
      ),
    );
  }
}