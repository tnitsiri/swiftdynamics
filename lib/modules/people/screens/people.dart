import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:objectdb/objectdb.dart';
import 'package:oktoast/oktoast.dart';
import 'package:path_provider/path_provider.dart';
// ignore: implementation_imports
import 'package:objectdb/src/objectdb_storage_filesystem.dart';
import 'package:swiftdynamics/models/person.dart';
import 'package:swiftdynamics/modules/people/components/item.dart';
import 'package:swiftdynamics/modules/people/screens/form.dart';
import 'package:swiftdynamics/modules/people/screens/info.dart';

class PeopleScreen extends StatefulWidget {
  @override
  State createState() => _PeopleScreenState();
}

class _PeopleScreenState extends State<PeopleScreen> {
  late ObjectDB _db;
  List<PersonModel> _items = [];
  
  // fetch list
  Future<bool> _fetch() async {
    bool isSuccess = false;

    try {
      List<dynamic> data = await _db.find();
      List<PersonModel> items = data.map((d) => PersonModel.fromJson(d)).toList();

      if (mounted) {
        setState(() {
          _items = items;
        });
      }

      isSuccess = true;
    }
    catch (_) {
      showToast('An error occurred during execution.');
    }

    return isSuccess;
  }

  // refresh
  Future<void> _onRefresh() async {
    await _fetch();
  }

  // add item
  void _add() async {
    dynamic response = await Navigator.push(context,
      CupertinoPageRoute(
        builder: (context) => PeopleFormScreen(),
      ),
    );

    if (response != null) {
      bool isSuccess = response;

      if (isSuccess) {
        _fetch();
      }
    }
  }

  // view detail
  void _view(PersonModel item) async {
    dynamic response = await Navigator.push(context,
      CupertinoPageRoute(
        builder: (context) => PeopleInfoScreen(
          item: item,
        ),
      ),
    );

    if (response != null) {
      bool isFetch = response;

      if (isFetch) {
        _fetch();
      }
    }
  }

  // remove item
  void _remove(PersonModel item) async {
    try {
      await _db.remove({
        'id': item.id,
      });

      _fetch();
    }
    catch (_) {
      showToast('An error occurred during execution.');
    }
  }

  // init database
  void _initDb() async {  
    try {
      Directory docDic = await getApplicationDocumentsDirectory();
      String dbPath = docDic.path + '/people.db';
      FileSystemStorage storage = FileSystemStorage(dbPath);
  
      _db = ObjectDB(storage);
      _fetch();
    }
    catch (_) {
    }
  }

  @override
  void initState() {
    _initDb();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('People'),
        trailing: CupertinoButton(
          minSize: 0,
          padding: EdgeInsets.zero,
          borderRadius: BorderRadius.zero,
          child: Text('Add'),
          onPressed: _add,
        ),
      ),
      child: CustomScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        slivers: [
          CupertinoSliverRefreshControl(
            onRefresh: _onRefresh,
          ),
          SliverFillRemaining(
            hasScrollBody: false,
            child: Container(
              padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).padding.bottom,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  ..._items.map((e) => PeopleItemComponent(
                    item: e,
                    onView: () => _view(e),
                    onRemove: () => _remove(e),
                  ))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}