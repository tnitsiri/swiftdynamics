import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:swiftdynamics/models/person.dart';
import 'package:swiftdynamics/modules/people/components/info_item.dart';

class PeopleInfoScreen extends StatefulWidget {
  final PersonModel item;

  PeopleInfoScreen({
    required this.item,
  });
  
  @override
  State createState() => _PeopleInfoScreenState();
}

class _PeopleInfoScreenState extends State<PeopleInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Personal Info'),
      ),
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).padding.bottom,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              PeopleInfoItemComponent(
                label: 'First name',
                value: widget.item.firstName,
              ),
              PeopleInfoItemComponent(
                label: 'Last name',
                value: widget.item.lastName,
              ),
              PeopleInfoItemComponent(
                label: 'About',
                value: widget.item.about,
              ),
              PeopleInfoItemComponent(
                label: 'Created at',
                value: DateFormat('dd/MMM/yy, HH:mm').format(widget.item.createdAt),
              ),
            ],
          ),
        ),
      ),
    );
  }
}