import 'package:flutter/cupertino.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:swiftdynamics/enums/calculator.dart';
import 'package:swiftdynamics/models/calculator.dart';
import 'package:swiftdynamics/modules/calculator/components/button.dart';
import 'package:swiftdynamics/modules/calculator/components/display.dart';
import 'package:uuid/uuid.dart';

class CalculatorScreen extends StatefulWidget {
  @override
  State createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  final Uuid _uuid = Uuid();
  final ScrollController _scrollController = ScrollController();
  final double _padding = 15;
  CalculatorState _state = CalculatorState.Number;
  List<CalculatorModel> _stacks = [];
  bool _isCalculated = false;
  bool _isError = false;

  // number
  void _number(int value) {
    if (_isError || _isCalculated) {
      _clear();
    }
    
    if (_stacks.last.state == CalculatorState.Number && _stacks.last.number != null) {
      if ((_stacks.last.number as String)[(_stacks.last.number as String).length - 1] == '.') {
        if (mounted) {
          setState(() {
            _stacks.last.number = (_stacks.last.number as String) + value.toString();
            _state = CalculatorState.Number;
            _isCalculated = false;
            _isError = false;
            _scrollController.jumpTo(0);
          });
        }
      }
      else {
        try {
          double number = double.parse(_stacks.last.number as String);

          if (number == 0 && value != 0 && mounted) {
            setState(() {
              _stacks.last.number = value.toString();
              _state = CalculatorState.Number;
              _isCalculated = false;
              _isError = false;
              _scrollController.jumpTo(0);
            });
          }
          else {
            bool allowed = !(_stacks.last.number as String).contains('.');

            if ((_stacks.last.number as String).contains('.')) {
              List<String> partial = (_stacks.last.number as String).split('.');

              if (partial.length == 2 && partial[1].length < 2) {
                allowed = true;
              }
            }

            if (allowed && mounted) {
              setState(() {
                _stacks.last.number = (_stacks.last.number as String) + value.toString();
                _state = CalculatorState.Number;
                _isCalculated = false;
                _isError = false;
                _scrollController.jumpTo(0);
              });
            }
          }
        }
        on FormatException catch (e) {
          if (_stacks.last.number as String == '-' && mounted) {
            setState(() {
              _stacks.last.number = (_stacks.last.number as String) + value.toString();
              _state = CalculatorState.Number;
              _isCalculated = false;
              _isError = false;
              _scrollController.jumpTo(0);
            });
          }
        }
        catch (_) {
        }
      }
    }
    else if (_stacks.last.state == CalculatorState.Operator) {
      if (mounted) {
        setState(() {
          _stacks.add(CalculatorModel(
            id: _uuid.v1(),
            state: CalculatorState.Number,
            number: value.toString(),
          ));
          _state = CalculatorState.Number;
          _isCalculated = false;
          _isError = false;
          _scrollController.jumpTo(0);
        });
      }
    }
  }

  // dot
  void _dot() {
    if (_state == CalculatorState.Operator) {
      if (mounted) {
        setState(() {
          _stacks.add(CalculatorModel(
            id: _uuid.v1(),
            state: CalculatorState.Number,
            number: '0.',
          ));
          _state = CalculatorState.Number;
          _isCalculated = false;
          _isError = false;
          _scrollController.jumpTo(0);
        });
      }
    }
    else if (_state == CalculatorState.Number && _stacks.last.number != null) {
      if (!(_stacks.last.number as String).contains('.')) {
        setState(() {
          _stacks.last.number = (_stacks.last.number as String) + '.';
          _state = CalculatorState.Number;
          _isCalculated = false;
          _isError = false;
          _scrollController.jumpTo(0);
        });
      }
    }
  }

  // operator
  void _operator(CalculatorOperator operator) {
    if (_state == CalculatorState.Operator && operator == CalculatorOperator.Subtraction) {
      if (mounted) {
        setState(() {
          _stacks.add(CalculatorModel(
            id: _uuid.v1(),
            state: CalculatorState.Number,
            number: '-',
          ));
          _state = CalculatorState.Number;
          _isCalculated = false;
          _isError = false;
          _scrollController.jumpTo(0);
        });
      }
    }
    else if (!_isError && _state == CalculatorState.Number) {
      double value = 0;

      try {
        double number = double.parse(_stacks.last.number as String);
        value = number;
      }
      catch (_) {
      }

      if (value == 0 && operator == CalculatorOperator.Subtraction) {
        if (mounted) {
          setState(() {
            _stacks.last.number = '-';
            _state = CalculatorState.Number;
            _isCalculated = false;
            _isError = false;
            _scrollController.jumpTo(0);
          });
        }
      }
      else {
        if (
          (value != 0 && _stacks.last.number != '-') ||
          (value == 0 && (_stacks.last.number == '0' || _stacks.last.number == '0.0'))
        ) {
          if (mounted) {
            setState(() {
              _stacks.add(CalculatorModel(
                id: _uuid.v1(),
                state: CalculatorState.Operator,
                operator: operator,
              ));
              _state = CalculatorState.Operator;
              _isCalculated = false;
              _isError = false;
              _scrollController.jumpTo(0);
            });
          }
        }
      }
    }
  }

  // calculate
  void _calculate() {
    if (!_isError && _stacks.length > 2) {
      try {
        List<String> data = [];

        for (int index = 0; index < _stacks.length; index++) {
          CalculatorModel stack = _stacks[index];

          if (stack.state == CalculatorState.Number) {
            try {
              double number = double.parse(stack.number as String);
              data.add(number.toString());
            }
            catch (_) {
            }
          }
          else if (stack.state == CalculatorState.Operator) {
            if (index != _stacks.length - 1) {
              if (stack.operator == CalculatorOperator.Addition) {
                data.add('+');
              }
              else if (stack.operator == CalculatorOperator.Subtraction) {
                data.add('-');
              }
              else if (stack.operator == CalculatorOperator.Multiplication) {
                data.add('*');
              }
              else if (stack.operator == CalculatorOperator.Division) {
                data.add('/');
              }
            }
          }
        }

        Parser parser = Parser();
        Expression expression = parser.parse(data.join(' '));
        ContextModel contextModel = ContextModel();
        double result = expression.evaluate(EvaluationType.REAL, contextModel);

        if (result == double.infinity) {
          throw new Error();
        }

        if (mounted) {
          setState(() {
            _stacks.clear();
            _stacks.add(CalculatorModel(
              id: _uuid.v1(),
              state: CalculatorState.Number,
              number: result.toString(),
            ));
            _state = CalculatorState.Number;
            _isCalculated = true;
            _isError = false;
            _scrollController.jumpTo(0);
          });
        }
      }
      catch (_) {
        if (mounted) {
          setState(() {
            _stacks.clear();
            _stacks.add(CalculatorModel(
              id: _uuid.v1(),
              state: CalculatorState.Error,
            ));
            _state = CalculatorState.Error;
            _isCalculated = false;
            _isError = true;
            _scrollController.jumpTo(0);
          });
        }
      }
    }
  }

  // clear
  void _clear() {
    if (mounted) {
      setState(() {
        _stacks.clear();
        _state = CalculatorState.Number;
        _isCalculated = false;
        _isError = false;
        _scrollController.jumpTo(0);
      });
    }

    _init();
  }

  void _init() {
    if (mounted) {
      setState(() {
        _stacks.add(CalculatorModel(
          id: _uuid.v1(),
          state: CalculatorState.Number,
          number: '0',
        ));
      });
    }
  }

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Calculator'),
        trailing: CupertinoButton(
          minSize: 0,
          padding: EdgeInsets.zero,
          borderRadius: BorderRadius.zero,
          child: Text('Clear'),
          onPressed: _clear,
        ),
      ),
      child: Container(
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).padding.bottom + _padding,
          left: _padding,
          right: _padding,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // start: display
            Expanded(
              child: Container(
                margin: EdgeInsets.only(
                  bottom: 20,
                ),
                child: SingleChildScrollView(
                  controller: _scrollController,
                  reverse: true,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        ..._stacks.map((e) => CalculatorDisplayComponent(
                          item: e,
                        )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            // end: display
            // start: inputs
            Container(
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  double spacing = 8;
                  int perRow = 4;
                  double size = (constraints.maxWidth / perRow) - spacing;

                  return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Wrap(
                        spacing: spacing,
                        runSpacing: spacing,
                        alignment: WrapAlignment.spaceBetween,
                        children: [
                          CalculatorButtonComponent(
                            size: size,
                            text: '7',
                            onPressed: () => _number(7),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '8',
                            onPressed: () => _number(8),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '9',
                            onPressed: () => _number(9),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '÷',
                            onPressed: () => _operator(CalculatorOperator.Division),
                            isOperator: true,
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '4',
                            onPressed: () => _number(4),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '5',
                            onPressed: () => _number(5),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '6',
                            onPressed: () => _number(6),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '×',
                            onPressed: () => _operator(CalculatorOperator.Multiplication),
                            isOperator: true,
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '1',
                            onPressed: () => _number(1),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '2',
                            onPressed: () => _number(2),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '3',
                            onPressed: () => _number(3),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '−',
                            onPressed: () => _operator(CalculatorOperator.Subtraction),
                            isOperator: true,
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '0',
                            onPressed: () => _number(0),
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '.',
                            onPressed: _dot,
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '=',
                            onPressed: _calculate,
                            isOperator: true,
                          ),
                          CalculatorButtonComponent(
                            size: size,
                            text: '+',
                            onPressed: () => _operator(CalculatorOperator.Addition),
                            isOperator: true,
                          ),
                        ],
                      ),
                    ],
                  );
                },
              ),
            ),
            // end: inputs
          ],
        ),
      ),
    );
  }
}