import 'package:flutter/cupertino.dart';
import 'package:swiftdynamics/enums/calculator.dart';
import 'package:swiftdynamics/models/calculator.dart';
import 'package:swiftdynamics/utils/numeral.dart';

class CalculatorDisplayComponent extends StatelessWidget {
  final CalculatorModel item;

  CalculatorDisplayComponent({
    required this.item,
  });

  @override
  Widget build(BuildContext context) {
    String text = '';

    if (item.state == CalculatorState.Number && item.number != null) {
      try {
        double number = double.parse(item.number as String);

        if (item.number == '0.') {
          text = item.number as String;
        }
        else if ((item.number as String)[(item.number as String).length - 1] == '.') {
          text = displayPrice(number) + '.';
        }
        else {
          text = displayPrice(number);
        }
      }
      on FormatException catch (e) {
        if (item.number as String == '-') {
          text = '-';
        }
      }
      catch (_) {
      }
    }
    else if (item.state == CalculatorState.Operator && item.operator != null) {
      if (item.operator == CalculatorOperator.Addition) {
        text = '+';
      }
      else if (item.operator == CalculatorOperator.Subtraction) {
        text = '−';
      }
      else if (item.operator == CalculatorOperator.Multiplication) {
        text = '×';
      }
      else if (item.operator == CalculatorOperator.Division) {
        text = '÷';
      }
    }
    else if (item.state == CalculatorState.Error) {
      text = 'ERROR';
    }

    return Container(
      child: Text(text,
        textAlign: TextAlign.end,
        style: TextStyle(
          color: item.state == CalculatorState.Error ? CupertinoColors.systemRed : CupertinoTheme.of(context).textTheme.textStyle.color,
          fontSize: item.state == CalculatorState.Operator ? 50 : 65,
        ),
      ),
    );
  }
}