import 'package:flutter/cupertino.dart';

class CalculatorButtonComponent extends StatelessWidget {
  final double size;
  final String text;
  final VoidCallback onPressed;
  final bool isOperator;

  CalculatorButtonComponent({
    required this.size,
    required this.text,
    required this.onPressed,
    this.isOperator = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      child: CupertinoButton(
        minSize: 0,
        padding: EdgeInsets.zero,
        borderRadius: BorderRadius.circular(size / 2),
        child: Container(
          decoration: BoxDecoration(
            color: isOperator ? CupertinoColors.systemOrange : Color(0xFF565656),
            shape: BoxShape.circle,
          ),
          child: Center(
            child: Text(text,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: CupertinoColors.white,
                fontSize: isOperator ? 36 : 30,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}