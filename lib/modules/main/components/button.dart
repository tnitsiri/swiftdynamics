import 'package:flutter/cupertino.dart';

class MainButtonComponent extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;

  MainButtonComponent({
    required this.text,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: CupertinoButton(
        child: Text(text,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16.3,
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}