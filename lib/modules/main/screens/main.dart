import 'package:flutter/cupertino.dart';
import 'package:swiftdynamics/modules/main/components/button.dart';

class MainScreen extends StatefulWidget {
  @override
  State createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Main'),
      ),
      child: Container(
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).padding.bottom,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            MainButtonComponent(
              text: 'Todo ',
              onPressed: () => Navigator.pushNamed(context, '/todo'),
            ),
            MainButtonComponent(
              text: 'Calculator ',
              onPressed: () => Navigator.pushNamed(context, '/calculator'),
            ),
            MainButtonComponent(
              text: 'People',
              onPressed: () => Navigator.pushNamed(context, '/people'),
            ),
          ],
        ),
      ),
    );
  }
}